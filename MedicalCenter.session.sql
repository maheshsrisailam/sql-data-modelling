create table doctors (
    id varchar(10) not null primary key,
    doctor_name varchar(120),
    specialization varchar(120)
);

insert into doctors 
(id,doctor_name,specialization)
values
("emp1001","James","Heart"),
("emp1002","Harry","Lungs"),
("emp1003","Martin","Skin"),
("emp1004","Hussaini","Brain");

create table patients (
    id varchar(10) not null primary key,
    patient_name varchar(120),
    age int,
    disease_related_to varchar(100)
);

insert into patients 
(id,patient_name,age,disease_related_to)
values
("pat2001","Ram",35,"Lungs"),
("pat2002","Rohit",48,"Heart"),
("pat2003","Harish",57,"Brain"),
("pat2004","Narine",33,"Skin");


create table consultation (
    id int not null primary key,
    patient_id varchar(10),
    disease varchar(120),
    doctor_id varchar(10),
    foreign key (patient_id) references patients(id) on delete cascade,
    foreign key (doctor_id) references doctors(id) on delete cascade
); 

insert into consultation 
(id,patient_id,disease,doctor_id)
values
(1,"pat2001","Heart","emp1001"),
(2,"pat2001", "Brain","emp1004"),
(3,"pat2002","Heart","emp1001"),
(4,"pat2003","Lungs","emp1002"),
(5,"pat2004","Skin","emp1003"),
(6,"pat2004","Brain","emp1004");


select patients.id as patient_id,
    patients.patient_name as patient_name,
    doctors.doctor_name as doctor_name,
    doctors.id as doctor_id
    from patients 
    inner join consultation 
    on patients.id = consultation.patient_id
    inner join doctors
    on consultation.doctor_id = doctors.id;

select count(patient_id) as patients_count,disease from consultation
group by disease
having patients_count >= 2
order by patients_count desc; 
