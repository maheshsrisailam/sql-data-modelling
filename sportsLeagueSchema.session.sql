
create table seasons (
    season_id int(4) not null primary key,
    season_start date,
    season_end date
);

create table teams (
    id varchar(10) not null primary key,
    team_name varchar(120)
);

create table players (
    player_id varchar(16) not null primary key,
    player_name varchar(120),
    player_type varchar(120),
    team_name varchar(120)
);

create table umpires (
    id varchar(16) not null primary key,
    umpire_name varchar(120)
);

create table matches (
    id int not null primary key,
    team1 varchar(120),
    team2 varchar(120),
    umpire1 varchar(120),
    umpire2 varchar(120),
    umpire3 varchar(120),
    season_id int(4),
    foreign key (season_id) references seasons(season_id) on delete cascade
);

create table scoreboardPerMatch (
    id int not null primary key,
    match_id int,
    player_name varchar(120),
    runs_scored int,
    wickets_taken int,
    foreign key (match_id) references matches(id) on delete cascade
);

create table pointsTable (
    team_name varchar(120),
    won int,
    lost int,
    no_result int,
    points int
);


insert into seasons (season_id,season_start,season_end)
values
(2008,'2008-03-25','2008-05-10'),
(2009,'2009-03-22','2009-05-08'),
(2010,'2010-03-28','2010-05-13'),
(2011,'2011-03-30','2011-05-15'),
(2012,'2012-03-25','2012-05-05'),
(2013,'2013-03-22','2013-05-04'),
(2014,'2014-03-24','2014-05-01'),
(2015,'2015-03-28','2015-05-08'),
(2016,'2016-03-29','2016-05-07'),
(2017,'2017-03-20','2017-05-10'),
(2018,'2018-03-25','2018-05-12'),
(2019,'2019-03-21','2019-05-11');



insert into teams
(id, team_name)
values 
("CSK","Chennai Super Kings"),
("MI","Mumbai Indians"),
("SRH","Sunrisers Hyderabad"),
("RCB","Royal ChallengersBanglore");


insert into players
(player_id,player_name,player_type,team_name)
VALUES
("PLAYER0001","MS Dhoni","Wicket-Keeper","Chennai Super Kings"),
("PLAYER0002","Virat Kohli","Batsman","Royal Challengers Banglore"),
("PLAYER0003","Holder","All-Rounder","Sunrisers Hyderabad"),
("PLAYER0004","Bumrah","Bowler","Mumbai Indians");

insert into matches 
(id,team1,team2,umpire1,umpire2,umpire3,season_id)
values
(1,"Chennai Super Kings","Mumbai Indians","Sundaram Ravi","Anil Chaudary","Paul Reiffel","2008"),
(2,"Royal Challengers Banglore","Sunrisers Hyderabad","Vineeth Kurkarni","Kumar Dharmaseena","Paul Reiffel","2008");


insert into scoreboardPerMatch
(id,match_id,player_name,runs_scored,wickets_taken)
values
(1,1,"MS Dhoni",47,0),
(2,1,"Jadeja",29,3);


insert into umpires VALUES
("UMPIRE0001","Sundaram Ravi"),
("UMPIRE0002","Anil Chaudary"),
("UMPIRE0003","Paul Reiffel"),
("UMPIRE0004","Vineeth Kurkarni"),
("UMPIRE0005","Kumar Dharmaseena");

insert into pointsTable
(team_name,won,lost,no_result,points)
values
("Chennai Super Kings",2,0,0,4),
("Mumbai Indians",1,1,0,2),
("Royal Challengers Banglore",1,1,0,2),
("Sunrisers Hyderabad",0,2,0,0);
