
drop database if exits craigslistDb;
create database craigslistDb;

create table users (
    id int not null primary key,
    user_name varchar(120),
    mobile varchar(10),
    region varchar(120)
);

insert into users (id,user_name,mobile,region)
values
(1,"Ram","8987456324","San Francisco"),
(2,"Seetha","7896542310","Seattle"),
(3,"Naveen","6897451230","Atlanta"),
(4,"Manjesh","7895642130","San Francisco"),
(5,"Dinesh","9875642130","Atlanta");


create table postcategories (
    id int not null primary key,
    category_type varchar(120)
);

insert into postcategories (id,category_type)
values
(1,"Greeting"),
(2,"Wish"),
(3,"Personal Message");


create table posts (
    id varchar(16) not null primary key,
    title varchar(120),
    post_text varchar(1000),
    user_id int,
    from_region varchar(120),
    to_loccation varchar(120),
    foreign key (user_id) references users(id) on delete cascade
);

insert into posts
(id,title,post_text,user_id,from_region,to_loccation)
values
("POST101","Greeting","Good Morning",1,"San Francisco","Seattle"),
("POST102","Wishes","Happy Birthday",2,"Seattle","San Francisco"),
("POST103","Personal Message","Received the post",2,"Seattle","Atlanta");



select 
    users.user_name,
    users.mobile,
    posts.title,
    posts.id
    from users
        inner join posts
            on users.id = posts.user_id;

select id, from_region from posts;

select id,title as post_category from posts;
